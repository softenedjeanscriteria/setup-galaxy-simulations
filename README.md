# Setup galaxy simulations from Ploeckinger et al ([arXiv](https://arxiv.org/abs/2310.10721))

## Description
Reproduce the isolated disk galaxy simulations from Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721)).

![Isolated disk galaxy](isolated_galaxy_examples.png "Stellar mass surface densities")


## Preparation

### Step 1: Install SWIFT
The isolated galaxy simulations presented in Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721)) use the [SWIFT](www.swiftsim.com) code (Schaller et al. [arXiv](https://arxiv.org/abs/2305.13380)). For reproducing these simulations install SWIFT following the information [here](https://swift.strw.leidenuniv.nl/docs/GettingStarted/index.html).

This repository contains the parameter file used in these simulations. Note, that in the future, the required parameters for the individual modules may change. After installing all [dependencies](https://swift.strw.leidenuniv.nl/docs/GettingStarted/compiling_code.html#dependencies) of SWIFT successfully, the SWIFT version used in Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721)) can be setup and compiled with

```
git clone https://github.com/SWIFTSIM/SWIFT.git
cd SWIFT
git checkout v0.9.0-1182-g423e9dd8
./autogen.sh 
./configure --with-subgrid=EAGLE-XL --with-hydro=sphenix --with-kernel=quartic-spline --with-ext-potential=hernquist --with-tbbmalloc --disable-hand-vec --enable-ipo --enable-stand-alone-fof
make
```

If the code compiled successfully, the executables `swift` and `fof` are produced in the SWIFT folder. 


### Step 2: Customize the simulation folder and start the simulation
At the path that the simulations should be run in, clone this reposity with

```
git clone https://gitlab.phaidra.org/softenedjeanscriteria/setup-galaxy-simulations.git
cd setup-galaxy-simulations
```

This repository contains the folder `fiducial_files` which requires the following changes:

- copy the `swift` and `fof` executables into the folder `fiducial_files`
- check the job submission `submit.job` and resubmission scripts `resubmit.job`, they might need updating or replacing, depending on the job submission system. The strings `RUNNAME` and `TIMEINHOURS` are replaced by the appropriate values by the `setup.py` script later.

In the folder `data`, run the script `getall.sh` to load all necessary data for the simulations (ICs, tables for cooling, photometry, and yields):

- `./getall.sh`

The `setup.py` script specifies which parameters are used. For Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721)), we varied:

- `mass_resolution_levels`: the mass resolution (level `M5` for a particle mass of 10<sup>5</sup>M<sub>sun</sub>, and `M6` for a particle mass of 8x10<sup>5</sup>M<sub>sun</sub>)
- `softening_resolutions_in_kpc`: the constant gravitational softening length (Plummer-equivalent softening length, &epsilon;) 
- `smoothing_resolutions_in_kpc`: the minimum smoothing length, h<sub>min</sub>
- `star_formation_efficiencies`: the star formation efficiency per (Newtonian) free-fall time

The default parameters start a subset of simulations with the `M6` mass resolution. These simulations are fast and can be used for testing. For the full set of simulations, comment and uncomment the relevant parameter blocks. 

The simulations with the specified parameters are set up **and** submitted with

```
python3 setup.py
```

### Step 3: Rerun the simulations with very small hmin 

We show in Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721)) that the SPH estimated densities are inaccurate representations of the particle positions, if the smoothing length, `h`, is limited by a minimum value, `hmin`. One of the simulation snapshots from the original simulation is converted into an initial conditions file (`data/create_ics_from_snapshot.py`) and the simulation is restarted with a very small value for `hmin` for one very short timestep. This is all done in the script `setup_reruns.py`:

- Select the mass resolution level(s) that should be re-run and the snapshot number (default: `snapshotnumber = 100`, t = 1 Gyr).

```
python3 setup_reruns.py
```

### Step 4: Identify stellar clumps with the stand-alone fof routine

For information on the stellar clumps that form within each simulation, the stand-alone FoF finder within SWIFT is used and setup with the script `setup_fof.py`.

- Select the mass resolution level(s) that should be re-run and the snapshot number (default: `snapshotnumber = 100`, t = 1 Gyr).

```
python3 setup_fof.py
```

In this particular version of SWIFT, some parts of the standalone FoF routine might not be available but we only use the `fof_output_0000.hdf5` file for the analysis here, which is produced successfully. 

### Step 5 Double-check and make plots

If all simulation have finished successfully, the following folders and files should be present per mass resolution (here shown for the small test parameter subset):

- `M6`: a subfolder is created for each run (e.g. `M6/GalaxyM6_soft0250pc_hmin007.75pc_sfe00.003`), containing the simulation snapshots (in particular: `output_0100.hdf5`)
- `M6_reruns_snap0100`: a subfolder is created for each run in `M6` with the prefix `Rerun` (e.g. `M6_reruns_snap0100/RerunGalaxyM6_soft0250pc_hmin007.75pc_sfe00.003`) containing the output file produced after rerunning the simulation for one short timestep (`output_0000.hdf5`)
- `M6_fofruns_snap0100`: a subfolder is created for each run in `M6` with the prefix `FOF` (e.g. `M6_reruns_snap0100/FOFGalaxyM6_soft0250pc_hmin007.75pc_sfe00.003`) containing the output file `fof_output_0000.hdf5`

**If all these files are present, all figures from Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721)) can be reproduced with the plotting scripts from [here](https://gitlab.phaidra.org/softenedjeanscriteria/analyse-galaxy-simulations).**

## Authors and acknowledgment
S. Ploeckinger, University of Vienna


