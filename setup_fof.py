import os
from pathlib import Path
import shutil
import glob
import re

##########################################
# Select mass resolution and snapshotnumber
##########################################
mass_resolution_levels = ["M5"]
snapshotnumber = 100
##########################################

localdir = os.getcwd()
for mass in mass_resolution_levels:
    os.chdir(os.path.join(localdir,mass))
    for runfolder in glob.glob("Galaxy*/"):
        ##########################################
        # setting runfolder name and creating rerunfolder
        ##########################################
        runfolder_absolute_path   = os.path.join(localdir, mass, runfolder)
        rerunfolder_absolute_path = os.path.join(localdir, mass+"_fofruns_snap%4.4i"%(snapshotnumber), "FOF"+runfolder)
        Path(rerunfolder_absolute_path).mkdir(parents=True, exist_ok=True)
        print (runfolder_absolute_path)

        ##########################################
        # change working directory to rerunfolder
        ##########################################
        os.chdir(rerunfolder_absolute_path)

        ##########################################
        # copy files into foffolder
        ##########################################
        shutil.copy2("../../fiducial_files/fof", "./")
        shutil.copy2("../../fiducial_files/fof.yml", "./")
        shutil.copy2(os.path.join(runfolder_absolute_path, "isolated_galaxy.yml"), "./isolated_galaxy_fof.yml")
        shutil.copy2(os.path.join(runfolder_absolute_path, "output_%4.4i.hdf5"%(int(snapshotnumber))), "./")

        ##########################################
        # update yml file for fof run
        ##########################################
        string_filename = "  file_name:               output_%4.4i.hdf5"%(int(snapshotnumber))

        f = open("isolated_galaxy_fof.yml", "rt")
        data = f.read()
        data = re.sub(r'^.*file_name.*$', string_filename, data, flags=re.MULTILINE)
        data = re.sub(r'^.*basename.*$', '  basename:    fof_group_output  ', data, flags=re.MULTILINE)
        f.close()

        f = open("fof.yml", "rt")
        datafof = f.read()
        f.close()

        f = open("isolated_galaxy_fof.yml", "wt")
        f.write(data)
        f.write(datafof)
        f.close()        

        ##########################################
        # start swift for one timestep
        ##########################################

        os.system("./fof --stars isolated_galaxy_fof.yml")










