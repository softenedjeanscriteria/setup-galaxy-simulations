#!/bin/bash 

# from Swift IsolatedGalaxy_feedback example (see Nobels et al. arXiv:2309.13750 for details)
wget http://virgodb.cosma.dur.ac.uk/swift-webstorage/ICs/IsolatedGalaxies_COLIBRE/M5_disk.hdf5
wget http://virgodb.cosma.dur.ac.uk/swift-webstorage/ICs/IsolatedGalaxies_COLIBRE/M6_disk.hdf5

